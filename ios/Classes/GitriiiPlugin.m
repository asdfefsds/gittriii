#import "GitriiiPlugin.h"
#if __has_include(<gitriii/gitriii-Swift.h>)
#import <gitriii/gitriii-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "gitriii-Swift.h"
#endif

@implementation GitriiiPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftGitriiiPlugin registerWithRegistrar:registrar];
}
@end
