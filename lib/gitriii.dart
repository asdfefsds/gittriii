
import 'dart:async';

import 'package:flutter/services.dart';

class Gitriii {
  static const MethodChannel _channel = MethodChannel('gitriii');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
